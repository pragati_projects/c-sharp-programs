using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Threading.Tasks;
using CampusSelectionRepository;
using CampusSelectionRepository.Repository.Models;
using CampusSelectionServices;

namespace CampusSelectionMockTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
           public async Task TestAddCandidate()
            {
                var mock = new Mock<ICampusSelectionR>();
                Candidate candidates = new Candidate();
                mock.Setup(p => p.AddCandidate(candidates)).ReturnsAsync(true);
                CampusSelectionS campusSelectionS = new CampusSelectionS(mock.Object);
                bool result = await campusSelectionS.AddCandidate(candidates);
                Assert.AreEqual(result, true);
            }
        [TestMethod]
        public async Task TestAddCollege()
        {
            var mock = new Mock<ICampusSelectionR>();
            College college = new College();
            mock.Setup(p => p.AddCollege(college)).ReturnsAsync(true);
            CampusSelectionS campusSelectionS = new CampusSelectionS(mock.Object);
            bool result = await campusSelectionS.AddCollege(college);
            Assert.AreEqual(result, true);
        }


    }
}
