﻿using CampusSelectionRepository.Repository.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CampusSelectionServices
{
    public interface ICampusSelectionS
    {
        Task<List<Candidate>> GetCandidateDetails();
        Task<bool> AddCandidate(Candidate candidates);
        Task<bool> AddCollege(College colleges);
    }
}
