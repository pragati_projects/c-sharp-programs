﻿using CampusSelectionRepository;
using CampusSelectionRepository.Repository.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CampusSelectionServices
{
    public class CampusSelectionS : ICampusSelectionS
    {
        private readonly ICampusSelectionR _icampusSelectionR;
        public CampusSelectionS(ICampusSelectionR campusSelectionR)
        {
            _icampusSelectionR = campusSelectionR;
        }

        public async Task<bool> AddCandidate(Candidate candidates)
        {
            return await _icampusSelectionR.AddCandidate(candidates);
        }

        public async Task<bool> AddCollege(College colleges)
        {
           return await _icampusSelectionR.AddCollege(colleges);
        }

        public async Task<List<Candidate>> GetCandidateDetails()
        {
            return await _icampusSelectionR.GetCandidateDetails();
        }
    }
}
