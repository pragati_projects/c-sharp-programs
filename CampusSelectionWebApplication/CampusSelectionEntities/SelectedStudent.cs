﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace CampusSelectionRepository.Repository.Models
{
    [Table("SelectedStudent")]
    public partial class SelectedStudent
    {
        [Key]
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public int CandidateId { get; set; }

        [ForeignKey(nameof(CandidateId))]
        [InverseProperty("SelectedStudents")]
        public virtual Candidate Candidate { get; set; }
        [ForeignKey(nameof(CompanyId))]
        [InverseProperty("SelectedStudents")]
        public virtual Company Company { get; set; }
    }
}
