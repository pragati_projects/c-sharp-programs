﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace CampusSelectionRepository.Repository.Models
{
    [Table("Company")]
    public partial class Company
    {
        public Company()
        {
            SelectedStudents = new HashSet<SelectedStudent>();
        }

        [Key]
        public int CompanyId { get; set; }
        [Required]
        [StringLength(50)]
        public string CompanyName { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime DateOfInterview { get; set; }

        [InverseProperty(nameof(SelectedStudent.Company))]
        public virtual ICollection<SelectedStudent> SelectedStudents { get; set; }
    }
}
