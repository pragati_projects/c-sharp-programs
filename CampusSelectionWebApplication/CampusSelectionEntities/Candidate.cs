﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace CampusSelectionRepository.Repository.Models
{
    [Table("Candidate")]
    public partial class Candidate
    {
        public Candidate()
        {
            SelectedStudents = new HashSet<SelectedStudent>();
        }

        [Key]
        public int CandidateId { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        [StringLength(50)]
        public string TechniccalSpecification { get; set; }
        [Required]
        [StringLength(50)]
        public string IsSelected { get; set; }
        public int CollegeId { get; set; }

        [ForeignKey(nameof(CollegeId))]
        [InverseProperty("Candidates")]
        public virtual College College { get; set; }
        [InverseProperty(nameof(SelectedStudent.Candidate))]
        public virtual ICollection<SelectedStudent> SelectedStudents { get; set; }
    }
}
