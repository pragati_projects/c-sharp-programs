﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace CampusSelectionRepository.Repository.Models
{
    [Table("College")]
    public partial class College
    {
        public College()
        {
            Candidates = new HashSet<Candidate>();
        }

        [Key]
        public int CollegeId { get; set; }
        [Required]
        [StringLength(50)]
        public string CollegeName { get; set; }
        [Required]
        [StringLength(50)]
        public string CollegeDescription { get; set; }

        [InverseProperty(nameof(Candidate.College))]
        public virtual ICollection<Candidate> Candidates { get; set; }
    }
}
