﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CampusSelectionWebApplication.Models
{
    public class SelectedStudentModel
    {
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public int CandidateId { get; set; }
    }
}
