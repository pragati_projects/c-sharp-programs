﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CampusSelectionWebApplication.Models
{
    public class CandidateModel
    {
        public int CandidateId { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        [StringLength(50)]
        public string TechniccalSpecification { get; set; }
        [Required]
        [StringLength(50)]
        public string IsSelected { get; set; }
        public int CollegeId { get; set; }

    }
}
