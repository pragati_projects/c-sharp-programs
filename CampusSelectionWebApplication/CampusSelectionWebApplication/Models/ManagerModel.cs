﻿using CampusSelectionRepository.Repository.Models;
using CampusSelectionServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CampusSelectionWebApplication.Models
{
    public class ManagerModel:IManagerModel
    {
        private readonly ICampusSelectionS _iCampusSelectionS;

        public ManagerModel(ICampusSelectionS campusSelectionS)
        {
            _iCampusSelectionS = campusSelectionS;
        }

        public async Task<bool> AddCandidate(CandidateModel candidateModel)
        {
            Candidate candidates = new Candidate();
            candidates.CandidateId = candidateModel.CandidateId;
            candidates.Name = candidateModel.Name;
            candidates.IsSelected = candidateModel.IsSelected;
            candidates.TechniccalSpecification = candidateModel.TechniccalSpecification;
            candidates.CollegeId = candidateModel.CollegeId;
            return await _iCampusSelectionS.AddCandidate(candidates);
        }

        public async Task<bool> AddCollege(CollegeModel collegeModel)
        {
            College colleges = new College();
            colleges.CollegeId = collegeModel.CollegeId;
            colleges.CollegeName = collegeModel.CollegeName;
            colleges.CollegeDescription = collegeModel.CollegeDescription;
            return await _iCampusSelectionS.AddCollege(colleges);
        }

        public CandidateModel candidateEntityToModel(Candidate candidates)
        {
            CandidateModel candidateModel = new CandidateModel();
            candidateModel.CandidateId = candidates.CandidateId;
            candidateModel.Name = candidates.Name;
            candidateModel.IsSelected = candidates.IsSelected;
            candidateModel.TechniccalSpecification = candidates.TechniccalSpecification;
            candidateModel.CollegeId = candidates.CollegeId;
            return candidateModel;
        }
        public async Task<List<CandidateModel>> GetCandidateDetails()
        {
            List<Candidate> candidateDetails = await _iCampusSelectionS.GetCandidateDetails();
            List<CandidateModel> candidateModels = new List<CandidateModel>();
            foreach(Candidate candidates in candidateDetails)
            {
                candidateModels.Add(candidateEntityToModel(candidates));
            }
            return candidateModels;
        }

        public CollegeModel collegeEntityToModel(College colleges)
        {
            CollegeModel collegeModel = new CollegeModel();
            collegeModel.CollegeId = colleges.CollegeId;
            collegeModel.CollegeName = colleges.CollegeName;
            collegeModel.CollegeDescription = colleges.CollegeDescription;
            return collegeModel;
        }

      

        public Task<List<CollegeModel>> GetCollegeDetails()
        {
            throw new NotImplementedException();
        }
    }
}
