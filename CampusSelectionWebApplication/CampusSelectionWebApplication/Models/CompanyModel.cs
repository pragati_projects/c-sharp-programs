﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CampusSelectionWebApplication.Models
{
    public class CompanyModel
    {
        public int CompanyId { get; set; }
        [Required]
        [StringLength(50)]
        public string CompanyName { get; set; }
       
        public DateTime DateOfInterview { get; set; }
    }
}
