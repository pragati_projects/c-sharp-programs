﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CampusSelectionWebApplication.Models
{
    public interface IManagerModel
    {
        Task<bool> AddCandidate(CandidateModel candidateModel);
        Task<List<CandidateModel>> GetCandidateDetails();
        Task<bool> AddCollege(CollegeModel collegeModel);
        Task<List<CollegeModel>> GetCollegeDetails();
    }
}
