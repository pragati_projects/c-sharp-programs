﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CampusSelectionWebApplication.Models
{
    public class CollegeModel
    {
        public int CollegeId { get; set; }
        [Required]
        [StringLength(50)]
        public string CollegeName { get; set; }
        [Required]
        [StringLength(50)]
        public string CollegeDescription { get; set; }
    }
}
