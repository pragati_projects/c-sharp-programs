﻿using CampusSelectionWebApplication.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CampusSelectionWebApplication.Controllers
{
    public class CompanyController : Controller
    {
        private readonly IManagerModel _managerModel;

        public CompanyController(IManagerModel managerModel)
        {
            _managerModel = managerModel;
        }
        public IActionResult Index()
        {
            return View();
        }
    }
}
