﻿using CampusSelectionCustomExceptions;
using CampusSelectionWebApplication.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CampusSelectionWebApplication.Controllers
{
    public class CandidateController : Controller
    {
        private readonly IManagerModel _managerModel;

        public CandidateController(IManagerModel managerModel)
        {
            _managerModel = managerModel;
        }

        public ActionResult AddCandidate()
        {
            return View();
        }

       
        [HttpPost]

        public async Task<IActionResult> AddCandidate(CandidateModel candidateModel)
        {
            try
            {
                if (await _managerModel.AddCandidate(candidateModel))
                {
                    ViewBag.Message = "Added Successfully";
                }
                else
                {
                    ViewBag.Message = "Couldnt Add";
                }
            }
            catch (SQlException ex)
            {
                ViewBag.Message = ex.Message;
            }

            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetCandidateDetails()
        {
            List<CandidateModel> candidateModel = await _managerModel.GetCandidateDetails();
            return View(candidateModel);
        }

       
    }
}
