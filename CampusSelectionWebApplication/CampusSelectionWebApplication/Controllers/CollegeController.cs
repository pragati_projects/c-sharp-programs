﻿using CampusSelectionWebApplication.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CampusSelectionWebApplication.Controllers
{
    public class CollegeController : Controller
    {
        private readonly IManagerModel _managerModel;

        public CollegeController(IManagerModel managerModel)
        {
            _managerModel = managerModel;
        }


        public ActionResult AddCollege()
        {
            return View();
        }


        [HttpPost]

        public async Task<IActionResult> AddCollege(CollegeModel collegeModel)
        {
            if (await _managerModel.AddCollege(collegeModel))
            {
                return View();
            }

            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetCollegeDetails()
        {
            List<CollegeModel> collegeModel = await _managerModel.GetCollegeDetails();
            return View(collegeModel);
        }

    }
}
