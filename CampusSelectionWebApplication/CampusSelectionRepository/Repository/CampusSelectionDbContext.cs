﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using CampusSelectionRepository.Repository.Models;

#nullable disable

namespace CampusSelectionRepository.Repository
{
    public partial class CampusSelectionDbContext : DbContext
    {
        public CampusSelectionDbContext()
        {
        }

        public CampusSelectionDbContext(DbContextOptions<CampusSelectionDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Candidate> Candidates { get; set; }
        public virtual DbSet<College> Colleges { get; set; }
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<SelectedStudent> SelectedStudents { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Data Source=.;Initial Catalog=CampusSelection;Integrated Security=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Candidate>(entity =>
            {
                entity.Property(e => e.IsSelected).IsUnicode(false);

                entity.Property(e => e.Name).IsUnicode(false);

                entity.Property(e => e.TechniccalSpecification).IsUnicode(false);

                entity.HasOne(d => d.College)
                    .WithMany(p => p.Candidates)
                    .HasForeignKey(d => d.CollegeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Candidate_College");
            });

            modelBuilder.Entity<College>(entity =>
            {
                entity.Property(e => e.CollegeDescription).IsUnicode(false);

                entity.Property(e => e.CollegeName).IsUnicode(false);
            });

            modelBuilder.Entity<Company>(entity =>
            {
                entity.Property(e => e.CompanyName).IsUnicode(false);
            });

            modelBuilder.Entity<SelectedStudent>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.CandidateId).ValueGeneratedOnAdd();

                entity.HasOne(d => d.Candidate)
                    .WithMany(p => p.SelectedStudents)
                    .HasForeignKey(d => d.CandidateId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SelectedStudent_Candidate");

                entity.HasOne(d => d.Company)
                    .WithMany(p => p.SelectedStudents)
                    .HasForeignKey(d => d.CompanyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SelectedStudent_Company");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
