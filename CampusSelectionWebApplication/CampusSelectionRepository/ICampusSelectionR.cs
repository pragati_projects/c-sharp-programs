﻿using CampusSelectionRepository.Repository.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CampusSelectionRepository
{
    public interface ICampusSelectionR
    {
        Task<bool> AddCandidate(Candidate candidates);
        Task<bool> AddCollege(College colleges);
        Task<List<Candidate>> GetCandidateDetails();
    }
}
