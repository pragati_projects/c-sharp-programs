﻿using CampusSelectionCustomExceptions;
using CampusSelectionRepository.Repository;
using CampusSelectionRepository.Repository.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CampusSelectionRepository
{
    public class CampusSelectionR : ICampusSelectionR
    {
        private readonly CampusSelectionDbContext _dbContext;

        public CampusSelectionR(CampusSelectionDbContext campusSelectionDbContext)
        {
            _dbContext = campusSelectionDbContext;
        }

        public async  Task<bool> AddCandidate(Candidate candidates)
        {
            try
            {
                _dbContext.Add(candidates);
                int rowsAffected = await _dbContext.SaveChangesAsync();
                if (rowsAffected == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch(SQlException ex)
            {
                throw new SQlException("Server problem", ex);
            }
        }

        public async Task<bool> AddCollege(College colleges)
        {
            _dbContext.Add(colleges);
            int rowsAffected = await _dbContext.SaveChangesAsync();
            if (rowsAffected == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public async Task<List<Candidate>> GetCandidateDetails()
        {
            List<Candidate> candidates = await _dbContext.Candidates.ToListAsync();
            return candidates;
        }
    }
}
