﻿using System;

namespace AccountApplication
{
    class Account
    {
      

        public int AccNo { get; set; }
        public string AccType { get; set; }
        public int DebitCard { get; set; }
        public int Pin { get; set; }
        public string LoginId { get; set; }
        public string Password { get; set; }
        public double Balance { get; set; }
    }

    class AccountMgr
    {
        public Account storeData(Account acc)
        {
          
            Console.WriteLine("enter acc no:");
            acc.AccNo = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("enter accType:");
            acc.AccType = Console.ReadLine();
            Console.WriteLine("enter debit card:");
            acc.DebitCard = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("enter pin:");
            acc.Pin = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("enter login id:");
            acc.LoginId = Console.ReadLine();
            Console.WriteLine("enter password:");
            acc.Password = Console.ReadLine();
            Console.WriteLine("enter balance:");
            acc.Balance= Convert.ToDouble(Console.ReadLine());
            
            return acc;
        }

        public void showData(Account acc)
        {
            Console.WriteLine(acc.AccNo);
            Console.WriteLine(acc.AccType);
            Console.WriteLine(acc.DebitCard);
            Console.WriteLine(acc.Pin);
            Console.WriteLine(acc.Password);
            Console.WriteLine(acc.Balance);
        }

       
    }

    class TestMain
    {
        static void Main(string[] args)
        {

            Account acc = new Account() ;
            AccountMgr obj = new AccountMgr();
            obj.storeData(acc);
            obj.showData(acc);

        }
    }
}
