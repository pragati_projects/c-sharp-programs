﻿using System;

namespace UserClass
{
    class Program
    {
        static void Main(string[] args)
        {
           
        }
    }

    public class User
    {
        private long userId;
        private string dateOfBirth;
        private string fullName;
        private string gender;
        private string emailId;
        private string password;

        public User(long userId, string dateOfBirth, string fullName, string gender, string emailId, string password)
        {
            UserId = userId;
            DateOfBirth = dateOfBirth;
            FullName = fullName;
            Gender = gender;
            EmailId = emailId;
            Password = password;



        }
        public long UserId { get; set; }
        public string DateOfBirth { get; set; }
        public string FullName { get; set; }
        public string Gender { get; set; }
        public string EmailId { get; set; }
        public string Password { get; set; }


    }
}
