﻿using MobileOperatorWebApplication.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MobileOperatorWebApplication.Controllers
{
    public class MobileOperatorController : Controller
    {
        ManagerModel managerModel = new ManagerModel();

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(MobileOperatorModel mobileOperatorModel)
        {
            managerModel.AddMobileOperator(mobileOperatorModel);
            return RedirectToAction("Index");
        }
        // GET: MobileOperator

        public ActionResult Index()
        {
            
            List<MobileOperatorModel> mobileOperatorModels = managerModel.GetAllOperators();

            return View(mobileOperatorModels);
        }


        public ActionResult TopTwo()
        {
            List<MobileOperatorModel> mobileOperatorModels = managerModel.GetTop2Operators();
            return View(mobileOperatorModels);
        }





    }
}