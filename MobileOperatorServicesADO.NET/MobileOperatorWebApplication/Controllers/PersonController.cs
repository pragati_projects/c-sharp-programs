﻿using MobileOperatorWebApplication.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MobileOperatorWebApplication.Controllers
{
    public class PersonController : Controller
    {
        ManagerModel managerModel = new ManagerModel();


        public ActionResult Create()
        {
            PersonModel person = new PersonModel();
            person.Mobile = managerModel.GetAllOperators();
            return View(person);
        }

        [HttpPost]
        public ActionResult Create(PersonModel personModel)
        {
            managerModel.AddPerson(personModel);
            return RedirectToAction("Index");
        }



        // GET: Person
        public ActionResult Index()
        {
            List<PersonModel> personModels = managerModel.GetAllPerson();

            return View(personModels);
        }
    }
}