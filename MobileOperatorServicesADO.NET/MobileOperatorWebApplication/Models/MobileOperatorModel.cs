﻿using System.ComponentModel.DataAnnotations;

namespace MobileOperatorWebApplication.Models
{
    public class MobileOperatorModel
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public int Ratings { get; set; }
    }
}