﻿using MobileOperatorBusinessLayer;
using MobileOperatorEntity;
using System.Collections.Generic;


namespace MobileOperatorWebApplication.Models
{
    public class ManagerModel
    {
        MobileOperatorBL mobileOperatorBL = new MobileOperatorBL();
        public void AddMobileOperator(MobileOperatorModel mobileOperatorModel)
        {
            MobileOperator mobileOperator = new MobileOperator();
            mobileOperator.Id = mobileOperatorModel.Id;
            mobileOperator.Name = mobileOperatorModel.Name;
            mobileOperator.Ratings = mobileOperatorModel.Ratings;
            mobileOperatorBL.AddMobileOperator(mobileOperator);
        }

        public MobileOperatorModel entityToModel(MobileOperator mobileOperator)
        {
            MobileOperatorModel mobileOperatorModel = new MobileOperatorModel();
            mobileOperatorModel.Id = mobileOperator.Id;
            mobileOperatorModel.Name = mobileOperator.Name;
            mobileOperatorModel.Ratings = mobileOperator.Ratings;
            return mobileOperatorModel;
        }

        public List<MobileOperatorModel> GetAllOperators()
        {
            List<MobileOperator> operatorDetails = MobileOperatorBL.GetAllOperators();
            List<MobileOperatorModel> operatorModels = new List<MobileOperatorModel>();
            foreach (MobileOperator mobileOperator in operatorDetails)
            {
                operatorModels.Add(entityToModel(mobileOperator));
            }
            return operatorModels;
        }

        public List<MobileOperatorModel> GetTop2Operators()
        {
            List<MobileOperator> operatorDetails = MobileOperatorBL.GetTop2Operators();
            List<MobileOperatorModel> operatorModels = new List<MobileOperatorModel>();
            foreach (MobileOperator mobileOperator in operatorDetails)
            {
                operatorModels.Add(entityToModel(mobileOperator));
            }
            return operatorModels;
        }

        public void AddPerson(PersonModel personModel)
        {
            Person person = new Person();
            person.PersonId = personModel.PersonId;
            person.PersonName = personModel.PersonName;
            person.Id = personModel.Id;
            mobileOperatorBL.AddPerson(person);
        }

        public PersonModel entityToModel1(Person person)
        {
            PersonModel personModel = new PersonModel();
            personModel.PersonId = person.PersonId;
            personModel.PersonName = person.PersonName;
            personModel.Id = person.Id;
            return personModel;
        }

        public List<PersonModel> GetAllPerson()
        {
            List<Person> personDetails = MobileOperatorBL.GetAllPerson();
            List<PersonModel> personModels = new List<PersonModel>();
            foreach (Person person in personDetails)
            {
                personModels.Add(entityToModel1(person));
            }
            return personModels;
        }

        //public void DisplayPersonById(int personId)
        //{
        //    Person person = new Person();
        //    Person personDetails = MobileOperatorBL.DisplayPersonById(personId);

        //    personDetails.Add(entityToModel1(personId));



        //}


    }
}


