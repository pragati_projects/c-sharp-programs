﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MobileOperatorWebApplication.Models
{
    public class PersonModel
    {
        [Required]
        public int PersonId { get; set; }
        [Required]
        public string PersonName { get; set; }
        [Required]

        [DisplayName("Operator")]
        public int Id { get; set; }

        [NotMapped]
        public List<MobileOperatorModel> Mobile { get; set; }
    }
}