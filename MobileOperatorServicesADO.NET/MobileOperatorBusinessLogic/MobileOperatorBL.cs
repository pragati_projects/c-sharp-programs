﻿using MobileOperatorDataAccess;
using MobileOperatorEntity;
using System.Collections.Generic;

namespace MobileOperatorBusinessLayer
{
    public class MobileOperatorBL
    {
        //MobileOperatorDA mobileOperatorDA = new MobileOperatorDA();
        public void AddMobileOperator(MobileOperator mobileOperator)
        {
            MobileOperatorDA.AddMobileOperator(mobileOperator);
        }

        public static List<MobileOperatorEntity.MobileOperator> GetAllOperators()
        {
            List<MobileOperatorEntity.MobileOperator> mobileOperator = new List<MobileOperatorEntity.MobileOperator>();
            mobileOperator = MobileOperatorDA.GetAllOperators();
            return mobileOperator;
        }

        public void AddPerson(Person person)
        {
            MobileOperatorDA.AddPerson(person);
        }

        public static List<Person> GetAllPerson()
        {
            List<Person> person = new List<Person>();
            person = MobileOperatorDA.GetAllPerson();
            return person;
        }

        public static List<MobileOperator> GetTop2Operators()
        {
            List<MobileOperator> mobileOperators = new List<MobileOperator>();
            mobileOperators = MobileOperatorDA.GetTop2Operators();
            return mobileOperators;
        }

        public static Person DisplayPersonById(int personId)
        {
            Person person = new Person();
            person = MobileOperatorDA.DisplayPersonById(personId);
            return person;
        }

        public static MobileOperator GetOperatorById(int id)
        {
            MobileOperator mobileOperator = new MobileOperator();
            mobileOperator = MobileOperatorBL.GetOperatorById(id);
            return mobileOperator;
        }
    }
}
