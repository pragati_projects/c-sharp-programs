﻿using System.Configuration;
using System.Data.SqlClient;

namespace MobileOperatorDataAccess
{
    class Connection
    {
        public static SqlConnection GetConnection()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["con"].ConnectionString;
            SqlConnection con = new SqlConnection(connectionString);
            return con;

        }
    }
}