﻿using MobileOperatorEntity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace MobileOperatorDataAccess
{
    public class MobileOperatorDA
    {
        public static string AddMobileOperator(MobileOperator mobileOperator)
        {
            string connectionString = "Data Source=.;Initial Catalog=Operator;Integrated Security=True";
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                // SqlConnection con = Connection.GetConnection();
                SqlCommand command = new SqlCommand("AddMobileOperator", con);
                con.Open();
                command.CommandType = System.Data.CommandType.StoredProcedure;


                SqlParameter parameter;
                parameter = command.Parameters.AddWithValue("@Id", mobileOperator.Id);
                parameter = command.Parameters.AddWithValue("@name", mobileOperator.Name);
                parameter = command.Parameters.AddWithValue("@rating", mobileOperator.Ratings);


                command.ExecuteNonQuery();
               // con.Close();
                return "Added Successfuly";
            }

        }

        public static List<MobileOperatorEntity.MobileOperator> GetAllOperators()
        {
            List<MobileOperatorEntity.MobileOperator> mobileOperator = new List<MobileOperatorEntity.MobileOperator>();
            SqlConnection con = Connection.GetConnection();

            con.Open();
            SqlCommand command1 = new SqlCommand("GetAllOperators", con);
            SqlDataReader reader = command1.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    MobileOperatorEntity.MobileOperator objMobile = new MobileOperatorEntity.MobileOperator();
                    objMobile.Id = Convert.ToInt32(reader[0]);
                    objMobile.Name = Convert.ToString(reader[1]);
                    objMobile.Ratings = Convert.ToInt32(reader[2]);

                    mobileOperator.Add(objMobile);



                }
            }
            con.Close();
            return mobileOperator;

        }
        public static string AddPerson(Person person)
        {
            SqlConnection con = Connection.GetConnection();

            con.Open();

            SqlCommand command = new SqlCommand("AddPerson", con);

            command.CommandType = System.Data.CommandType.StoredProcedure;
            SqlParameter parameter;

            parameter = command.Parameters.AddWithValue("@PersonId", person.PersonId);
            parameter = command.Parameters.AddWithValue("@personName", person.PersonName);
            parameter = command.Parameters.AddWithValue("@Id", person.Id);

            command.ExecuteNonQuery();
            con.Close();

            return "Added Successfuly";
        }

        public static List<Person> GetAllPerson()
        {
            List<Person> person = new List<Person>();
            SqlConnection con = Connection.GetConnection();

            con.Open();
            SqlCommand command1 = new SqlCommand("GetAllPerson", con);
            SqlDataReader reader = command1.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Person objPerson = new Person();
                    objPerson.PersonId = Convert.ToInt32(reader[0]);
                    objPerson.PersonName = Convert.ToString(reader[1]);
                    objPerson.Id = Convert.ToInt32(reader[2]);

                    person.Add(objPerson);
                }
            }
            con.Close();
            return person;
        }

        public static List<MobileOperator> GetTop2Operators()
        {
            List<MobileOperator> mobileOperators = new List<MobileOperator>();
            SqlConnection con = Connection.GetConnection();

            con.Open();
            SqlCommand command1 = new SqlCommand("GetTop2Operators", con);
            SqlDataReader reader = command1.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    MobileOperator objMobile = new MobileOperator();
                    objMobile.Id = Convert.ToInt32(reader[0]);
                    objMobile.Name = Convert.ToString(reader[1]);
                    objMobile.Ratings = Convert.ToInt32(reader[2]);

                    mobileOperators.Add(objMobile);
                }
            }
            con.Close();
            return mobileOperators;

        }
        public static MobileOperator GetOperatorById(int id)
        {
            SqlConnection con = Connection.GetConnection();

            con.Open();


            SqlCommand command = new SqlCommand("spDisplayOperatorById", con);

            command.CommandType = System.Data.CommandType.StoredProcedure;
            SqlParameter param;

            param = command.Parameters.AddWithValue("@Id", id);

            command.ExecuteNonQuery();

            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    MobileOperator objOperator = new MobileOperator();
                    objOperator.Id = Convert.ToInt32(reader[0]);
                    objOperator.Name = Convert.ToString(reader[1]);
                    objOperator.Ratings = Convert.ToInt32(reader[2]);



                    return objOperator;
                }
            }
            return null;


        }

        public static Person DisplayPersonById(int personId)
        {
            SqlConnection con = Connection.GetConnection();

            con.Open();
            SqlCommand command;

            string sqlQuery = string.Format("spDisplayPersonById");
            command = new SqlCommand(sqlQuery, con);
            command.CommandType = System.Data.CommandType.StoredProcedure;
            con.Open();
            SqlParameter parameter;
            parameter = command.Parameters.AddWithValue("@PersonId", personId);

            command.ExecuteNonQuery();

            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Person objPerson = new Person();
                    objPerson.PersonId = Convert.ToInt32(reader[0]);
                    objPerson.PersonName = Convert.ToString(reader[1]);
                    objPerson.Id = Convert.ToInt32(reader[2]);

                    return objPerson;
                }
            }
            return null;
        }
    }
}

