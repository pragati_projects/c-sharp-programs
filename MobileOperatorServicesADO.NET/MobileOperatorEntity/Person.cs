﻿namespace MobileOperatorEntity
{
    public class Person
    {
        public int PersonId { get; set; }
        public string PersonName { get; set; }
        public int Id { get; set; }
    }
}
