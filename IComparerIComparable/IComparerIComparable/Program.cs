﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace IComparerIComparable
{

    public class Student : IComparable<Student>
    {
        public int Sid { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public float Marks { get; set; }

        public int CompareTo(Student other)
        {
            if (this.Sid > other.Sid)
                return 1;
            else if (this.Sid < other.Sid)
                return -1;
            else
                return 0;
        }
    }
    class CompareStudents : IComparer<Student>
    {
        public int Compare( Student x,  Student y)
        {
            //if (x.Marks > y.Marks)
            //    return 1;
            //else if (x.Marks < y.Marks)
            //    return -1;
            //else
            //    return 0;

            return x.Marks.CompareTo(y.Marks);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Student students1 = new Student { Sid = 101, Name = "Pragati ", Age = 23, Marks = 575.00f };

            Student students2 = new Student { Sid = 109, Name = "Stuart ", Age = 22, Marks = 592.00f };

            Student students3 = new Student { Sid = 107, Name = "David ", Age = 30, Marks = 412.00f };

            Student students4 = new Student { Sid = 103, Name = "Williams", Age = 30, Marks = 390.00f };

            List<Student> students = new List<Student>() { students1, students2 , students3,students4}; //collection initializer
            CompareStudents obj = new CompareStudents();              //create instance of class
       //   students.Sort();    //for sorting icomparerable
            students.Sort(obj);
         // students.Reverse();            //to reverse
            foreach(Student items in students)
            {
                Console.WriteLine(items.Sid + " " + items.Name + " " + items.Age + " " + items.Marks);
            }

            Console.ReadLine();
        }
    }
}
