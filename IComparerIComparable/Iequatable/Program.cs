﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iequatable
{

    class Program
    {
        static void Main(string[] args)
        {
            
            var studentA = new Student() { ID = 1, Name = "Radha", GPA = 2.75F };
            var studentB = new Student() { ID = 2, Name = "Seetha", GPA = 3.4F };
            var studentC = new Student() { ID = 1, Name = "Suresh", GPA = 2.71F };

            var AtoB = studentA.Equals(studentB);
            var AtoC = studentA.Equals(studentC);

            Console.WriteLine("Student A is equal to Student B = {0}", AtoB);
            Console.WriteLine("Student A is equal to Student C = {0}", AtoC);

            Console.ReadLine();
        }
    }
}

public class Student : IEquatable<Student>
{
    public int ID { get; set; }
    public string Name { get; set; }
    public float GPA { get; set; }

    public bool Equals(Student otherStudent)
    {
        return (this.ID == otherStudent.ID && this.Name == otherStudent.Name && this.GPA == otherStudent.GPA);

    }
}

