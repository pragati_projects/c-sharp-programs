﻿using System;

namespace CodingChallenge
{
    class Program
    {
        public void menu()
        {
            Console.WriteLine("Enter1.To add Track");
            Console.WriteLine("Enter 2.To add capability to system");
            Console.WriteLine("enter 3 to display all capabilities as per track");
        }

        public Capability[] AddCapability(Capability[] obj1)
        {
            for (int i = 0; i < obj1.Length; i++) { 
            Console.WriteLine("Enter capability id:");
            int capabilityId = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter summary");
            string Summary = Console.ReadLine();
            Console.WriteLine("Enter KO capabilities:");
            string ko = Console.ReadLine();
            Console.WriteLine("Enter non Ko capabilties");
            string nonKo = Console.ReadLine();
            obj1[i] = new Capability();
                obj1[i].CapabilityId = capabilityId;
                obj1[i].KO = ko;
                obj1[i].NonKo = nonKo;

        }
            return obj1;
    }
       

        static void Main(string[] args)
        {
            Console.WriteLine("Enter the size:");
            int size = Convert.ToInt32(Console.ReadLine());
            Capability[] obj1 = new Capability[size];
            Track[] obj = new Track[3]; 
            Program obj2 = new Program();
            int i = 0;


            int choice;
            do
            {
                obj2.menu();
                Console.WriteLine("Enter choice:");
                choice = Convert.ToInt32(Console.ReadLine());

                switch (choice)
                {
                    case 1:
                        
                        Console.WriteLine("Enter trak id:");
                       
                        try
                        {
                            obj[i].TrackId = Convert.ToInt32(Console.ReadLine());

                            if (obj[i].TrackId > 0)
                            {
                                Console.WriteLine("Track id accepetd" + obj[i].TrackId);
                            }
                            else
                            {
                                throw (new InvalidTrackCode("Track code should always be positive"));
                            }
                        }
                        catch(InvalidTrackCode ex)
                        {
                            Console.WriteLine(ex.Message.ToString());
                        }
                        
                        Console.WriteLine("Enter name:");
                        obj[i].Name = Console.ReadLine();
                        
                        try
                        {
                           for(int j = 0; j <= i; j++)
                            {
                                if (obj[i].Name.CompareTo(obj[j].Name)>0){
                                    Console.WriteLine("Name accepted");


                                }
                                else
                                {
                                    throw (new TrackNameExists("The track name is present"));
                                }


                             }
                        }
                        catch(TrackNameExists ex)
                        {
                            Console.WriteLine(ex.Message.ToString());
                        }
                        break;

                    case 2: obj2.AddCapability(obj1);
                            break;
                }
            } while (choice != 4);
            
        }
    }

    public class Capability
    {
        
        public int CapabilityId { get; set; }
        public string Summary { get; set; }
        public string KO { get; set; }
        public string NonKo { get; set; }
        Track obj = new Track();
    }

    public class Track
    {
       
        public int TrackId { get; set; }
        public string Name { get; set; }
    }
}

//creating custom exception
public class TrackNameExists : Exception
{
    public TrackNameExists(string message) : base(message)
    {

    }
    
}

public class InvalidTrackCode : Exception
{
    public InvalidTrackCode(string message) : base(message)
    {

    }

}
