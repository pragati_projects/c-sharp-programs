﻿using System;

namespace RoomBookingCustomExceptions
{
    public class NoDataFoundException : Exception
    {
        public NoDataFoundException()
        {

        }

        public NoDataFoundException(string msg) : base(msg)
        {

        }
        public NoDataFoundException(string msg, Exception innerException) : base(msg, innerException)
        {

        }
    }
}
