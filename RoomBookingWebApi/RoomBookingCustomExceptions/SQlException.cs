﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RoomBookingCustomExceptions
{
    public class SQlException: Exception
    {
        public SQlException()
        {

        }
        public SQlException(string msg): base(msg)
        {

        }

        public SQlException(string msg, Exception innerException) : base(msg, innerException)
        {

        }
    }
}
