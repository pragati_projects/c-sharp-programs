﻿using RoomBookingEntites.Repository.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RoomBookingBusinessLayer
{
    public interface IRoomBookingBL
    {
        Task<bool> AddCustomer(Customer customers);
        Task<List<Customer>> GetCustomerDetails();
        Task<bool> UpdateCustomer(Customer customers);
        Task<bool> AddHotel(Hotel hotels);
        Task<List<Hotel>> GetHotelDetails();
        Task<bool> AddRoom(Room rooms);
        Task<List<Hotel>> GetListOfHotelsBasedOnCity(string city);
        Task<List<Room>> GetRoomDetails();
        Task<bool> UpdatingIsDelete(int id);
    }
}
