﻿using RoomBookingDataAccessLayer;
using RoomBookingEntites.Repository.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RoomBookingBusinessLayer
{
    public class RoomBookingBL : IRoomBookingBL
    {
        private readonly IRoomBookingDAL _roomBookingDAL;
        public RoomBookingBL(IRoomBookingDAL roomBookingDAL)
        {
            _roomBookingDAL = roomBookingDAL;
        }

        public async Task<bool> AddCustomer(Customer customers)
        {
            return await _roomBookingDAL.AddCustomer(customers);
        }

        public async Task<bool> AddHotel(Hotel hotels)
        {
            return await _roomBookingDAL.AddHotel(hotels);
        }

        public async Task<bool> AddRoom(Room rooms)
        {
            return await _roomBookingDAL.AddRoom(rooms);
        }

        public async  Task<List<Customer>> GetCustomerDetails()
        {
            return await _roomBookingDAL.GetCustomerDetails();
        }

        public async Task<List<Hotel>> GetHotelDetails()
        {
            return await _roomBookingDAL.GetHotelDetails();
        }

        public async Task<List<Hotel>> GetListOfHotelsBasedOnCity(string city)
        {
            return await _roomBookingDAL.GetListOfHotelBasedOnCity(city);
        }

        public async Task<List<Room>> GetRoomDetails()
        {
            return await _roomBookingDAL.GetRoomDetails();
        }

        public async Task<bool> UpdateCustomer(Customer customers)
        {
            return await _roomBookingDAL.UpdateCustomer(customers);
        }

        public async Task<bool> UpdatingIsDelete(int id)
        {
            return await _roomBookingDAL.UpdatingIsDelete(id);
        }
    }
}
