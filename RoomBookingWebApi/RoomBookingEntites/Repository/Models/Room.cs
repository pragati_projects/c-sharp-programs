﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace RoomBookingEntites.Repository.Models
{
    [Table("Room")]
    public partial class Room
    {
        [Key]
        public int RoomId { get; set; }
        public int RoomNo { get; set; }
        [Required]
        [StringLength(50)]
        public string RoomStatus { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime FromDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime ToDate { get; set; }
        public int CustomerId { get; set; }
        public int HoteId { get; set; }
        public bool IsDeleted { get; set; }

        [ForeignKey(nameof(CustomerId))]
        [InverseProperty("Rooms")]
        public virtual Customer Customer { get; set; }
        [ForeignKey(nameof(HoteId))]
        [InverseProperty(nameof(Hotel.Rooms))]
        public virtual Hotel Hote { get; set; }
    }
}
