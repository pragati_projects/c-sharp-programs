﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace RoomBookingEntites.Repository.Models
{
    [Table("Customer")]
    public partial class Customer
    {
        public Customer()
        {
            Rooms = new HashSet<Room>();
        }

        [Key]
        public int CustomerId { get; set; }
        [Required]
        [StringLength(50)]
        public string CustomerName { get; set; }
        [Required]
        [StringLength(50)]
        public string Address { get; set; }
        [Required]
        [StringLength(50)]
        public string GovtIdProof { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        [StringLength(10)]
        public string PhoneNumber { get; set; }

        [InverseProperty(nameof(Room.Customer))]
        public virtual ICollection<Room> Rooms { get; set; }
    }
}
