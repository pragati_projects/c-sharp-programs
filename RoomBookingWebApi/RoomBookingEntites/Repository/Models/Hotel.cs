﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace RoomBookingEntites.Repository.Models
{
    [Table("Hotel")]
    public partial class Hotel
    {
        public Hotel()
        {
            Rooms = new HashSet<Room>();
        }

        [Key]
        public int HotelId { get; set; }
        [Required]
        [StringLength(50)]
        public string HotelName { get; set; }
        public int RoomPrice { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime CheckIn { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime CheckOut { get; set; }
        [Required]
        [StringLength(50)]
        public string City { get; set; }

        [InverseProperty(nameof(Room.Hote))]
        public virtual ICollection<Room> Rooms { get; set; }
    }
}
