﻿using Microsoft.EntityFrameworkCore;
using RoomBookingEntites.Repository;
using RoomBookingEntites.Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RoomBookingCustomExceptions;

namespace RoomBookingDataAccessLayer
{
    public class RoomBookingDAL : IRoomBookingDAL
    {
        private readonly RoomBookingDbContext _dbContext;
        public RoomBookingDAL(RoomBookingDbContext roomBookingDbContext)
        {
            _dbContext = roomBookingDbContext;
        }

        public async Task<bool> AddCustomer(Customer customers)
        {
            try
            {
                int rowsAffected = 0;
                _dbContext.Add(customers);
                rowsAffected = await _dbContext.SaveChangesAsync();
                if (rowsAffected == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch(Exception ex)
            {
                throw new SQlException("Sorry could not connect", ex);
            }
        }

        public async Task<bool> AddHotel(Hotel hotels)
        {
            int rowsAffected = 0;
            _dbContext.Add(hotels);
            rowsAffected = await _dbContext.SaveChangesAsync();
            if (rowsAffected == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public async Task<bool> AddRoom(Room rooms)
        {
            int rowsAffected = 0;
            _dbContext.Add(rooms);
            rowsAffected = await _dbContext.SaveChangesAsync();
            if (rowsAffected == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public async Task<List<Customer>> GetCustomerDetails()
        {
            try
            {
                List<Customer> customers = await _dbContext.Customers.ToListAsync();
                if (customers == null)
                {
                    throw new NoDataFoundException("No customers are present!");
                }
                else 
                {
                    return customers;
                }
            }
            catch(NoDataFoundException ex)
            {
                throw new NoDataFoundException("No data present", ex);
            }
        }

        public async Task<List<Hotel>> GetHotelDetails()
        {
            List<Hotel> hotels = await _dbContext.Hotels.ToListAsync();
            return hotels;
        }

        public async Task<List<Hotel>> GetListOfHotelBasedOnCity(string city)
        {
            List<Hotel> hotels = await _dbContext.Hotels.Where(h => h.City == city).ToListAsync();
            return hotels;
        }

        public async Task<List<Room>> GetRoomDetails()
        {
            List<Room> rooms = await _dbContext.Rooms.ToListAsync();
            return rooms;
        }

        public async Task<bool> UpdateCustomer(Customer customers)
        {
            try
            {
                Customer customerTemp = await _dbContext.Customers.FirstOrDefaultAsync(c => c.CustomerId == customers.CustomerId);
                if (customerTemp != null)
                {

                    customerTemp.Address = customers.Address;
                    customerTemp.Email = customers.Email;
                    customerTemp.PhoneNumber = customers.PhoneNumber;
                    int rowsAffected = 0;
                    rowsAffected = await _dbContext.SaveChangesAsync();
                    if (rowsAffected == 0)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    throw new NoDataFoundException("Sorry! Data youre searching for is not present");
                }
            }
            catch(NoDataFoundException ex)
            {
                throw new NoDataFoundException("Please add data", ex);
            }
            catch(SQlException ex)
            {
                throw new SQlException("sorry!Could not connect", ex);
            }
        }

        public async Task<bool> UpdatingIsDelete(int id)
        {
            Room roomTemp = await _dbContext.Rooms.FirstOrDefaultAsync(r => r.RoomId == id);
            roomTemp.IsDeleted = false;
            roomTemp.RoomStatus = "available";
            int rowsAffected = 0;
            rowsAffected = await _dbContext.SaveChangesAsync();
            if (rowsAffected == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
