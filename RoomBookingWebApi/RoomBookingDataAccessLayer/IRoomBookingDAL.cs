﻿using RoomBookingEntites.Repository.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RoomBookingDataAccessLayer
{
    public interface IRoomBookingDAL
    {
        Task<bool> AddCustomer(Customer customers);
        Task<bool> AddHotel(Hotel hotels);
        Task<bool> AddRoom(Room rooms);
        Task<List<Customer>> GetCustomerDetails();
        Task<List<Hotel>> GetHotelDetails();
        Task<List<Hotel>> GetListOfHotelBasedOnCity(string city);
        Task<List<Room>> GetRoomDetails();
        Task<bool> UpdateCustomer(Customer customers);
        Task<bool> UpdatingIsDelete(int id);
    }
}
