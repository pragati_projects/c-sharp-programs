﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RoomBookingBusinessLayer;
using RoomBookingEntites.Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoomBookingWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HotelController : ControllerBase
    {
        private readonly IRoomBookingBL _roomBookingBL;
        public HotelController(IRoomBookingBL roomBookingBL)
        {
            _roomBookingBL = roomBookingBL;
        }

        [HttpPost]
        public async Task<IActionResult> AddHotel(Hotel hotels)
        {
            return Ok(await _roomBookingBL.AddHotel(hotels));
        }

        [HttpGet]
        public async Task<IActionResult> GetHotelDetails()
        {
            return Ok(await _roomBookingBL.GetHotelDetails());
        }

        [HttpGet("ListOfHotelsBasedOnCity")]
        public async Task<IActionResult> GetListOfHotelsBasedOnCity(string city)
        {
            return Ok(await _roomBookingBL.GetListOfHotelsBasedOnCity(city));
        }
    }
}
