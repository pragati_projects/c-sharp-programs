﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RoomBookingBusinessLayer;
using RoomBookingCustomExceptions;
using RoomBookingEntites.Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoomBookingWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly IRoomBookingBL _roomBookingBL;
        public CustomerController(IRoomBookingBL roomBookingBL)
        {
            _roomBookingBL = roomBookingBL;
        }

        [HttpPost]
        public async Task<IActionResult> AddCustomer(Customer customers)
        {
            try
            {
                return Ok(await _roomBookingBL.AddCustomer(customers));
            }
            catch(SQlException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetCustomerDetails()
        {
            try
            {
                return Ok(await _roomBookingBL.GetCustomerDetails());
            }
            catch(NoDataFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("Update Customer")]
        public async Task<IActionResult> UpdateCustomer(Customer customers)
        {
            try
            {
                return Ok(await _roomBookingBL.UpdateCustomer(customers));
            }
            catch(NoDataFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch(SQlException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
