﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RoomBookingBusinessLayer;
using RoomBookingEntites.Repository.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoomBookingWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoomController : ControllerBase
    {
        private readonly IRoomBookingBL _roomBookingBL;
        public RoomController(IRoomBookingBL roomBookingBL)
        {
            _roomBookingBL = roomBookingBL;
        }

        [HttpPost]
        public async Task<IActionResult> AddRoom(Room rooms)
        {
            return Ok(await _roomBookingBL.AddRoom(rooms));
        }

        [HttpGet]
        public async Task<IActionResult> GetRoomDetails()
        {
            return Ok(await _roomBookingBL.GetRoomDetails());
        }
        [HttpPost("Update")]
        public async Task<IActionResult> UpdatingISDelete(int id)
        {
            return Ok(await _roomBookingBL.UpdatingIsDelete(id));
        }
    }
}
