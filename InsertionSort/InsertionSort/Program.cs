﻿using System;

namespace InsertionSort
{
    class Program
    {
        static void Main(string[] args)
        {
            Program obj = new Program();
            Console.WriteLine("Enter the size");
            int size = Convert.ToInt32(Console.ReadLine());
            int[] array = new int[size];
            
            Console.WriteLine("enter array elements:");
            for(int i = 0; i < size; i++)
            {
               
               
                //Console.WriteLine(i);
                array[i]= Convert.ToInt32(Console.ReadLine());
            }
            Console.WriteLine("the values stored into array are:");
            for(int i = 0; i < size; i++)
            {
                Console.WriteLine(array[i]);
            }

            int[] result = obj.InsertionSort(size, array);
            Console.Write("the sorted elements are:\n");
            for (int i = 0; i < result.Length; i++)
            {
               
                Console.WriteLine(result[i]);

            }
            Console.ReadLine();




        }

        public int[] InsertionSort(int size, int[] array)
        {
            for(int i = 1; i < size; i++)
            {
                int key = array[i];
                int j = i - 1;
                while(j>=0 && key <= array[j])
                {
                    array[j + 1] = array[j];
                    j--;
                }
                array[j + 1] = key;



            }
            return array;
        }
    }
}
