﻿using System;

namespace Practice1
{
    class Program
    {
        static void Main(string[] args)
        {
            int num;
            Console.WriteLine("Please enter a number:");
            num = Convert.ToInt32(Console.ReadLine());
            int res = check_prime(num);
            if (res == 0)
            {
                Console.WriteLine("its not a prime number" + num);
            }
            else
            {
                Console.WriteLine("it is a prime number" + num);
            }

        }

        private static int check_prime(int num)
        {
            int i;
            for( i = 2; i <= num - 1; i++)
            {
                if (num % i == 0)
                {
                    return 0;
                }
            }
            if (i== num)
                {
                    return 1;
                }
            return 0;
        }
    }
}
