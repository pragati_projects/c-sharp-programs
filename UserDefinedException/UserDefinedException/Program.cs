﻿using System;
using System.Linq;

namespace UserDefinedException
{
    class Program
    {
        public void ValidateId(long id) 
        {
            long temp = id;
            int count = 0;
            while (temp > 0)
            {
                temp = temp / 10;
                count++;
            }
            
           
                if (count > 5)
                {
                    Console.WriteLine("User id is acepted" + id);
                }
                else
                {
                    throw (new InValidUserIdException("User id length should be greater than 5 digits"));
                }
            
        }

        public void ValidatePassword(string password)
        {
            if(password.Length<5 && password.Any(char.IsUpper) && password.Any(char.IsLower) && password.Any(char.IsDigit))
            {
                Console.WriteLine("Password accepted" + password);
            }
            else
            {
                throw (new InValidPasswordException("User password should contain uppercase lowercase alphabets and a digit and should be of length 5"));
            }
        }

        

        public void Menu()
        {
            Console.WriteLine("ENTER 1. To add Details");
            Console.WriteLine("ENTER 2.To Display Details");
            Console.WriteLine("Enter 3. Exit");
        }

        public Handling[] CreateUser(Handling[] obj)
        {
            for(int i = 0; i < obj.Length; i++)
            {
                int flag = 0;
                long id = 0;
                while (flag == 0)
                {
                    Console.WriteLine("enter User id:");
                   

                    try
                    {
                        id = Convert.ToInt64(Console.ReadLine());
                        ValidateId(id);
                        flag = 1;
                    }
                    catch (InValidUserIdException ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
                Console.WriteLine("enter date of birth:");
                string dateOfBirth = Console.ReadLine();

                Console.WriteLine("enter full name:");
                string fullName = Console.ReadLine();

                Console.WriteLine("enter email id:");
                string emailId = Console.ReadLine();

                Console.WriteLine("enter password:");
                string password = " ";
                try
                {
                    password = Console.ReadLine();
                    ValidatePassword(password);
                }
                catch(InValidPasswordException ex)
                {
                    Console.WriteLine(ex.Message);
                }

                obj[i] = new Handling();
                obj[i].Id = id;
                obj[i].DateOfBirth = dateOfBirth;
                obj[i].FullName = fullName;
                obj[i].EmailId = emailId;
                obj[i].Password = password;
            }
            return obj;
            
           
        }

        public void ShowDetails(Handling[] obj)
        {
            for(int i = 0; i < obj.Length; i++)
            {
                Console.WriteLine("/nId is:" + " " + obj[i].Id + "/nDate of birth is:" + " " + obj[i].DateOfBirth + "/nFull Name is:" + " " +
                    obj[i].FullName + "/nEmail id is:" + " " + obj[i].EmailId + "Password is:" + obj[i].Password);
            }
        }

       
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the size:");
            int size = Convert.ToInt32(Console.ReadLine());

            Handling[] obj = new Handling[size];
            Program obj1 = new Program();

            int choice;
            do
            {
                obj1.Menu();
                Console.WriteLine("Enter your choice:");
                choice = 1;
                try
                {
                    choice = Convert.ToInt32(Console.ReadLine());


                    if (choice <= 0 || choice >= 5)
                    {
                        throw new InValidChoiceException("Invalid Choice");
                    }



                    switch (choice)
                    {
                        case 1:
                            obj1.CreateUser(obj);
                            break;

                        case 2:
                            obj1.ShowDetails(obj);
                            break;

                        case 3:
                            Console.WriteLine("EXITING....");
                            break;


                    }
                }
                catch (InValidChoiceException ex)
                {
                    Console.WriteLine(ex.Message);
                }
            } while (choice != 3);

           

        }

        public class Handling
        {
            public long Id { get; set; }
            public  string DateOfBirth { get; set; }
            public string FullName { get; set; }
            public string EmailId{ get; set; }
            public string Password { get; set; }
        }
    }


  

   

    //creating custom exception
    public class InValidUserIdException : Exception
    {
        public InValidUserIdException(string message) : base(message){

        }
    }

    public class InValidPasswordException : Exception
    {
        public InValidPasswordException(string message) : base(message)
        {

        }
    }

    public class InValidChoiceException : Exception
    {
        public InValidChoiceException(string message): base(message)
        {

        }
    }
}
