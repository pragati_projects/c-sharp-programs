﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Documentary.Entity;

namespace Documentary.Repository
{
    public interface IDocumentRepsoitory
    {
        Task<bool> AddDocumentaryDetails(Document document);
        Task<List<Document>> DisplayDocumentaryDetails();
    }
}
