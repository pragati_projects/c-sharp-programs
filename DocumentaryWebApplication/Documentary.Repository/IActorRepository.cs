﻿using Documentary.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Documentary.Repository
{
    public interface IActorRepository
    {
        Task<bool> AddActor(Actor actor);
        Task<List<Actor>> DisplayActorDetails();
    }
}
