﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Documentary.Entity;

namespace Documentary.Repository
{
    public class DocumentaryDbContext : DbContext
    {
        public DocumentaryDbContext(DbContextOptions<DocumentaryDbContext> options) : base(options)
        {

        }
        public DbSet<Actor> Actors { get; set; }

        public DbSet<Document> Documentaries { get; set; }

       



    }
}

