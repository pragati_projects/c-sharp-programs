﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Documentary.Entity;
using Microsoft.EntityFrameworkCore;
using CustomException;

namespace Documentary.Repository
{
    public class ActorRepository: IActorRepository
    {
        private readonly DocumentaryDbContext _documentaryDbContext;

        public ActorRepository(DocumentaryDbContext documentaryDb)
        {
            _documentaryDbContext = documentaryDb;
        }

        public async Task<bool> AddActor(Actor actor)
        {
            int EntryAdded = 0;

            try
            {
                _documentaryDbContext.Add(actor);
                EntryAdded = await _documentaryDbContext.SaveChangesAsync();

                if (EntryAdded == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch(Exception ex)
            {
                throw new SQlException("Sorry! Could not connect. Please try again", ex);
            }
        }

        public async Task<List<Actor>> DisplayActorDetails()
        {
            try
            {
                List<Actor> actor = await _documentaryDbContext.Actors.ToListAsync();
                return actor;
            }
            catch(Exception ex)
            {
                throw new SQlException("Sorry! List was not found. Please try after some time", ex);
            }
        }
    }
}
