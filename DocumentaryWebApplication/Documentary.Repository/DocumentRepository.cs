﻿using Documentary.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomException;
using Microsoft.EntityFrameworkCore;

namespace Documentary.Repository
{
    public class DocumentRepository : IDocumentRepsoitory
    {
        private readonly DocumentaryDbContext _documentDBContext;

        public DocumentRepository(DocumentaryDbContext documentaryDbContext)
        {
            _documentDBContext = documentaryDbContext;
        }

        public async Task<bool> AddDocumentaryDetails(Document document)
        {
            int documentEntry = 0;
            try
            {
                _documentDBContext.Add(document);
                documentEntry = await _documentDBContext.SaveChangesAsync();

                if (documentEntry == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw new SQlException("Sorry!Could not connect. Please try again !", ex);
            }
            
        }

        public async Task<List<Document>> DisplayDocumentaryDetails()
        {
            try
            {
                List<Document> document = await _documentDBContext.Documentaries.ToListAsync();
                return document;
            }
            catch(Exception ex)
            {
                throw new SQlException("Sorry!Could not load your result as its empty! Please try again", ex);
            }
        }
    }
}
