﻿using Documentary.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Documentary.Entity;

namespace DocumentaryWebApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActorController : ControllerBase
    {
        private readonly IActorServices _aservices;

        public ActorController(IActorServices actorServices)
        {
            _aservices = actorServices;
        }

        [HttpPost]
        public async Task<IActionResult> AddActor(Actor actor)
        {
            return Ok(await _aservices.AddActor(actor));
        }

        [HttpGet]
        public async Task<IActionResult> DisplayActorDetails()
        {
            return Ok(await _aservices.DisplayActorDetails());
        }

    }
}
