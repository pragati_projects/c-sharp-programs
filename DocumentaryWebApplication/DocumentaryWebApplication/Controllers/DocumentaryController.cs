﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Documentary.Services;
using Documentary.Entity;

namespace DocumentaryWebApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentaryController : ControllerBase
    {
        private readonly IDocumentServices _dservices;

        public DocumentaryController(IDocumentServices documentServices)
        {
            _dservices = documentServices;
        }

        [HttpPost]
        public async Task<IActionResult> AddDocumentaryDetails(Document document)
        {
            return Ok(await _dservices.AddDocumentaryDetails(document));
        }

        [HttpGet]
        public async Task<IActionResult> DisplayDocumentaryDetails()
        {
            return Ok(await _dservices.DisplayDocumentaryDetails());
        }
    }
}
