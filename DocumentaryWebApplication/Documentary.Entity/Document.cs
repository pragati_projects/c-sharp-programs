﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Documentary.Entity
{
    public class Document
    {
        [Key]
        [Required]
        public int DocumentaryId { get; set; }

        [Required]
        public string DocumentaryName { get; set; }

        [Required]
        public string DocumentaryGenre { get; set; }

        [ForeignKey("ActorId")]
        public int ActorId { get; set; }
    }
}
