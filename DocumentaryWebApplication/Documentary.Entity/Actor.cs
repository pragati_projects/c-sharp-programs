﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Documentary.Entity
{
    public class Actor
    {
        [Key]
        [Required]
        public int ActorId { get; set; }

        [Required]
        public string ActorName { get; set; }

        [Required]
        public int ActorAge { get; set; }

        [Required]
        public DateTime DateOfBirth { get; set; }
    }
}
