﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Documentary.Entity;
using Documentary.Repository;

namespace Documentary.Services
{
    public class ActorServices : IActorServices
    {
        private readonly IActorRepository _arepository;

        public ActorServices(IActorRepository actorRepository)
        {
            _arepository = actorRepository;
        }

        public async Task<bool> AddActor(Actor actor)
        {
            return await _arepository.AddActor(actor);
        }

        public async Task<List<Actor>> DisplayActorDetails()
        {
            return await _arepository.DisplayActorDetails();
        }
    }
}
