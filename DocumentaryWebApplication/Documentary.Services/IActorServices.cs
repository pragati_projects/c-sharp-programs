﻿using Documentary.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Documentary.Services
{
    public interface IActorServices
    {
        Task<bool> AddActor(Actor actor);
        Task<List<Actor>> DisplayActorDetails();
    }
}
