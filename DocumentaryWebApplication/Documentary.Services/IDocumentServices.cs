﻿using Documentary.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Documentary.Services
{
    public interface IDocumentServices
    {
        Task<bool> AddDocumentaryDetails(Document document);
        Task<List<Document>> DisplayDocumentaryDetails();
    }
}
