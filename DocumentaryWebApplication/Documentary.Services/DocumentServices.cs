﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Documentary.Repository;
using Documentary.Entity;

namespace Documentary.Services
{
    public class DocumentServices : IDocumentServices

    {
        private readonly IDocumentRepsoitory _drepository;

        public DocumentServices(IDocumentRepsoitory documentRepsoitory)
        {
            _drepository = documentRepsoitory;
        }

        public async Task<bool> AddDocumentaryDetails(Document document)
        {
            return await _drepository.AddDocumentaryDetails(document);
        }

        public async Task<List<Document>> DisplayDocumentaryDetails()
        {
            return await _drepository.DisplayDocumentaryDetails();
        }
    }
}
