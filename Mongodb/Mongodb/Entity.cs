﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mongodb
{
    public class Entity
    {
        [BsonId]
        public ObjectId MemberId { get; set; }

        [BsonElement("Name")]
        public string MemberName { get; set; }

        [BsonElement("Age")]
        public int Age { get; set; }

        [BsonElement("Salary")]
        public int Salary { get; set; }

        public Entity(string MemberName, int Age, int Salary)
        {
            this.MemberName = MemberName;
            this.Age = Age;
            this.Salary = Salary;
        }
    }
}

