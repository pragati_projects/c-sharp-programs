﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;

namespace Mongodb
{
    public class Program
    {
        static MongoClient mongoClient = new MongoClient();
        static IMongoDatabase memberDb = mongoClient.GetDatabase("MemberDb"); // create
        static IMongoCollection<Entity> membersCollection = memberDb.GetCollection<Entity>("Members"); //create
        static void Main(string[] args)
        {
            Entity member = new Entity("Kaushik", 350, 1);
            //GetMemberByName();
            //string result = UpdateMember();
            //Console.WriteLine(result);
            GetAllMembers();
             //DeleteAll();
            // UpdateMember();
            //InsertMember(member);
            // DeleteMember();
        }

        private static void GetMemberByName()
        {
            var member = membersCollection.Find(
                       Builders<Entity>.Filter.Eq("Name", "Jimin")).ToList<Entity>();
            foreach (var item in member)
            {
                Console.WriteLine(item.MemberName + ":" + item.Salary);
            }
        }

        private static void DeleteMember()
        {
            membersCollection.DeleteOne(s => s.MemberName == "JYP");
        }
        private static void DeleteAll()
        {
            List<Entity> members = membersCollection.AsQueryable().ToList<Entity>();
            if (members.Count != 0)
            {
                foreach (var member in members)
                {
                    var obj = member;
                    membersCollection.DeleteOne(s => s.MemberName == obj.MemberName);
                }
                Console.WriteLine("ALL DETAILS DELETED");
            }
            else
                Console.WriteLine("No Data Present !!!");
        }
        private static string UpdateMember()
        {
            var updateMember = Builders<Entity>.Update.Set(s => s.MemberName, "Jongin");
            membersCollection.UpdateOne(s => s.MemberName == "Kai", updateMember);
            return "Member Details Updated";
        }

        private static void InsertMember(Entity member)
        {
            membersCollection.InsertOne(member);
            



        }

        private static void GetAllMembers()
        {
            List<Entity> members = membersCollection.AsQueryable().ToList<Entity>();
            if (members.Count != 0)
            {
                foreach (var member in members)
                {
                    Console.WriteLine(member.MemberName + ":" + member.Salary);
                }
            }
            else
                Console.WriteLine("NO DATA PRESENT !!");
        }
    }
   
}
