﻿using System;

namespace Practice4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("enter a string:");
            String str = Console.ReadLine();
            int res = check_vowels(str);
            Console.WriteLine(res);

        }

        public static int check_vowels(String str)
        {
            int count = 0;
            for(int i = 0; i < str.Length; i++)
            {
                if(str[i]=='a' || str[i]=='e' || str[i]=='i' || str[i]=='o' || str[i]=='u' || str[i] == 'A' || str[i] == 'E' || str[i] == 'I' || str[i] == 'O' || str[i] == 'U')
                {
                    count++;
                }
            }
            return count;
        }


    }
}
