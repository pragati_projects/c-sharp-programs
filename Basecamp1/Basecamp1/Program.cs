﻿using System;

namespace Basecamp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //bool  = true;
            Program myObject = new Program();
            Console.WriteLine("Please enter a input string:");
            String str = Console.ReadLine();
            Console.WriteLine("enter the 2nd string:");
            String str1 = Console.ReadLine();
            bool res = myObject.IsRotation(str, str1);
            if(res)
            {
                Console.WriteLine("strings are rotation of each other");
            }
            else
            {
                Console.WriteLine("strings are not of rotation of each other");
            }



        }

        public Boolean IsRotation(String str, String str1)
        {
            if (str.Length != str1.Length)
            {
                return false;
            }
            int index = str1.IndexOf(str[0]);
            if (index > -1)
            {                   // index throws -1 if substring not found

                for (int i = 0; i < str1.Length; i++)
                {
                    if (index == str1.Length)
                        index = 0;
                    if (str[i] == str1[index])
                    {
                        index++;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
                return false;

            return true;
        }
    }



}


