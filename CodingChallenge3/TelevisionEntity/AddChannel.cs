﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelevisionEntity
{
    public class AddChannel
    {
        public string ChannelName { get; set; }
        public string ChannelType { get; set; }
        public string Description { get; set; }
    }
}
