﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelevisionEntity
{
    public class AddTvPrograms
    {
        public string Name { get; set; }
        public string Category { get; set; }
        public string ChannelName { get; set; }
        public string Description { get; set; }
        public int Time { get; set; }
    }
}
