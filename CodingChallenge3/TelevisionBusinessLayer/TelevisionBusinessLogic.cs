﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelevisionEntity;
using TelevisionDataAccessLayer;

namespace TelevisionBusinessLayer
{
    public class TelevisionBusinessLogic
    {
        public void AddTelevision(AddChannel addChannel)
        {
            TelevisionDAL.AddTelevision(addChannel);
        }

        public static List<AddChannel> GetAllChannels()
        {
            List<AddChannel> addChannel = new List<AddChannel>();
            addChannel = TelevisionDAL.GetAllChannels();
            return addChannel;
        }

        public void AddPrograms(AddTvPrograms addTvPrograms)
        {
            TelevisionDAL.AddPrograms(addTvPrograms);
        }

        public static List<AddTvPrograms> GetAllPrograms()
        {
            List<AddTvPrograms> addTvPrograms = new List<AddTvPrograms>();
            addTvPrograms = TelevisionDAL.GetAllPrograms();
            return addTvPrograms;
        }


    }
}
