﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TelevsionWebApplication.Models;

namespace TelevsionWebApplication.Controllers
{
    public class AddChannelController : Controller
    {
        ManageModel manageModel = new ManageModel();

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(AddChannelModel addChannelModel)
        {
            
                manageModel.AddTelevision(addChannelModel);
                return RedirectToAction("Index");
            
           
           
        }
        // GET: AddChannel
        public ActionResult Index()
        {
            
                List<AddChannelModel> addChannelModels = manageModel.GetAllChannels();
                
                    return View(addChannelModels);
                
            
           

            
        }
    }
}