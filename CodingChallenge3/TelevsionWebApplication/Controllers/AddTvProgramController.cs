﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TelevsionWebApplication.Models;

namespace TelevsionWebApplication.Controllers
{
    public class AddTvProgramController : Controller
    {
        ManageModel manageModel = new ManageModel();
        
        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Create(AddTvProgramModel addTvProgramModel)
        {
            manageModel.AddPrograms(addTvProgramModel);
            return RedirectToAction("Index");
        }


        // GET: AddTvProgram
        public ActionResult Index()
        {
            try
            {
                List<AddTvProgramModel> addTvProgramModels = manageModel.GetAllPrograms();
                if (addTvProgramModels != null)
                    return View(addTvProgramModels);
                else
                    throw new Exception();
            }
            catch (Exception)
            {
                return RedirectToAction("Error");
            }


        }
    }
}