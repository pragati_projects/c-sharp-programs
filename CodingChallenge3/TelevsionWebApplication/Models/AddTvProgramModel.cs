﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TelevsionWebApplication.Models
{
    public class AddTvProgramModel
    {
        public string Name { get; set; }
        public string Category { get; set; }
        public string ChannelName { get; set; }
        public string Description { get; set; }
        public int Time { get; set; }
    }
}