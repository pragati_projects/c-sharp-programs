﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TelevisionEntity;
using TelevisionBusinessLayer;
namespace TelevsionWebApplication.Models
{
    public class ManageModel
    {
        TelevisionBusinessLogic televisionBusinessLogic = new TelevisionBusinessLogic();

        //Mapping takes place here in order to convert entity to model //
        public void AddTelevision(AddChannelModel addChannelModel)
        {
            AddChannel addChannel = new AddChannel();
            addChannel.ChannelName = addChannelModel.ChannelName;
            addChannel.ChannelType = addChannelModel.ChannelType;
            addChannel.Description = addChannelModel.Description;

            televisionBusinessLogic.AddTelevision(addChannel);
        }

        public AddChannelModel entityToModel(AddChannel addChannel)
        {
            AddChannelModel addChannelModel = new AddChannelModel();
            addChannelModel.ChannelName = addChannel.ChannelName;
            addChannelModel.ChannelType = addChannel.ChannelType;
            addChannelModel.Description = addChannel.Description;
            
            return addChannelModel;
        }

        public List<AddChannelModel> GetAllChannels()
        {
            List<AddChannel> channelDetails = TelevisionBusinessLogic.GetAllChannels();
            List<AddChannelModel> channelModels = new List<AddChannelModel>();
            foreach (AddChannel addChannel in channelDetails)
            {
                channelModels.Add(entityToModel(addChannel));
            }
            return channelModels;
        }

        public void AddPrograms(AddTvProgramModel addTvProgramModel)
        {
            AddTvPrograms addTvPrograms = new AddTvPrograms();
            addTvPrograms.Name = addTvProgramModel.Name;
            addTvPrograms.Category = addTvProgramModel.Category;
            addTvPrograms.ChannelName = addTvProgramModel.ChannelName;
            addTvPrograms.Description = addTvProgramModel.Description;
            addTvPrograms.Time = addTvProgramModel.Time;
           

            televisionBusinessLogic.AddPrograms(addTvPrograms);
        }
        public AddTvProgramModel entityToModel1(AddTvPrograms addTvPrograms)
        {
            AddTvProgramModel addTvProgramModel = new AddTvProgramModel();
            addTvProgramModel.Name = addTvPrograms.Name;
            addTvProgramModel.Category = addTvPrograms.Category;
            addTvProgramModel.ChannelName = addTvPrograms.ChannelName;
            addTvProgramModel.Description = addTvPrograms.Description;
            addTvProgramModel.Time = addTvPrograms.Time;
           

            return addTvProgramModel;
        }

        public List<AddTvProgramModel> GetAllPrograms()
        {
            List<AddTvPrograms> programDetails = TelevisionBusinessLogic.GetAllPrograms();
            List<AddTvProgramModel> programModels = new List<AddTvProgramModel>();
            foreach (AddTvPrograms addTvPrograms in programDetails)
            {
                programModels.Add(entityToModel1(addTvPrograms));
            }
            return programModels;
        }


    }
}