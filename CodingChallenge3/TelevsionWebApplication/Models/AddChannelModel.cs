﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TelevsionWebApplication.Models
{
    public class AddChannelModel
    {
        public string ChannelName { get; set; }
        public string ChannelType { get; set; }
        public string Description { get; set; }
    }
}