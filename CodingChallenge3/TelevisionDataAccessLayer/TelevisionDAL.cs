﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelevisionEntity;
using System.Data.SqlClient;
using System.Data;

namespace TelevisionDataAccessLayer
{
    public class TelevisionDAL
    {
        public static string AddTelevision(AddChannel addChannel)
        {
            SqlConnection con = Connection.GetConnection();
            try
            {
                SqlCommand command = new SqlCommand("spAddChannel", con);
                con.Open();
                command.CommandType = System.Data.CommandType.StoredProcedure;


                SqlParameter parameter;
                parameter = command.Parameters.AddWithValue("@CName", addChannel.ChannelName);
                parameter = command.Parameters.AddWithValue("@Ctype", addChannel.ChannelType);
                parameter = command.Parameters.AddWithValue("@Description", addChannel.Description);


                command.ExecuteNonQuery();
            }
            catch(Exception eX)
            {
                return eX.Message;
            }
            finally
            {
                con.Close();
            }
           
            return "Added Successfuly";


        }

        public static List<AddChannel> GetAllChannels()
        {
            List<AddChannel> addChannel = new List<AddChannel>();
            SqlConnection con = Connection.GetConnection();

            con.Open();
            try
            {
                SqlCommand command1 = new SqlCommand("spDisplay", con);
                SqlDataReader reader = command1.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        AddChannel objChannel = new AddChannel();
                        objChannel.ChannelName = Convert.ToString(reader[0]);
                        objChannel.ChannelType = Convert.ToString(reader[1]);
                        objChannel.Description = Convert.ToString(reader[2]);


                        addChannel.Add(objChannel);

                        

                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            finally
            {
                con.Close();
            }
            
            return addChannel;

        }
        public static string AddPrograms(AddTvPrograms addTvProgram)
        {
            SqlConnection con = Connection.GetConnection();
            try
            {
                SqlCommand command = new SqlCommand("spAddTvProgram", con);
                con.Open();
                command.CommandType = System.Data.CommandType.StoredProcedure;


                SqlParameter parameter;
                parameter = command.Parameters.AddWithValue("@Name", addTvProgram.Name);
                parameter = command.Parameters.AddWithValue("@Category", addTvProgram.Category);
                parameter = command.Parameters.AddWithValue("@CName", addTvProgram.ChannelName);
                parameter = command.Parameters.AddWithValue("@Description", addTvProgram.Description);
                parameter = command.Parameters.AddWithValue("@Duration", addTvProgram.Time);

    


                command.ExecuteNonQuery();
            }
            catch (Exception eX)
            {
                return eX.Message;
            }
            finally
            {
                con.Close();
            }

            return "Added Successfuly";


        }

        public static List<AddTvPrograms> GetAllPrograms()
        {
            List<AddTvPrograms> addTvPrograms = new List<AddTvPrograms>();
            SqlConnection con = Connection.GetConnection();

            con.Open();
            try
            {
                SqlCommand command1 = new SqlCommand("spDisplayProgram", con);
                SqlDataReader reader = command1.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        AddTvPrograms objProgram = new AddTvPrograms();
                        objProgram.Name = Convert.ToString(reader[0]);
                        objProgram.Category = Convert.ToString(reader[1]);
                        objProgram.ChannelName = Convert.ToString(reader[2]);
                        objProgram.Description = Convert.ToString(reader[3]);
                        objProgram.Time = Convert.ToInt32(reader[4]);
                       

                        addTvPrograms.Add(objProgram);



                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            finally
            {
                con.Close();
            }

            return addTvPrograms;

        }

    }
}
